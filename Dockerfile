FROM php:7.4-fpm-alpine

WORKDIR /var/www/html
ADD . /var/www/html

# phpの設定を反映
COPY ./docker/php/*.ini /usr/local/etc/php/conf.d/

# extension等の追加
RUN apk update && apk add --no-cache \
    autoconf \
    freetype-dev \
    git \
    gcc \
    g++ \
    vim \
    unzip \
    tzdata \
    libmcrypt-dev \
    libltdl \
    curl-dev \
    libxml2-dev \
    postgresql-dev \
    libpng-dev \
    libjpeg-turbo-dev \
    zip \
    libzip-dev \
    unzip \
    gmp-dev \
    icu-dev \
    openldap-dev \
    make \
    supervisor \
    pcre-dev \
  && git clone https://github.com/phpredis/phpredis.git /usr/src/php/ext/redis \
  && docker-php-ext-install -j$(nproc) dba gd gmp intl ldap mysqli opcache xmlrpc pdo_mysql iconv redis \
  && pecl install xdebug apcu igbinary mcrypt msgpack oauth uuid zip \
  && docker-php-ext-enable xdebug apcu igbinary mcrypt msgpack oauth uuid zip \
  && cp /usr/share/zoneinfo/Asia/Tokyo /etc/localtime \
  && apk del tzdata \
  && rm -rf /var/cache/apk/*

# composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer \
  && composer config -g repos.packagist composer https://packagist.jp \
  && composer global require hirak/prestissimo
#RUN composer install

#RUN cp .env.example .env \
#  && php artisan key:generate

EXPOSE 9000

# php-fpmの実行
COPY ./docker/supervisor/supervisord.conf /etc/supervisor/conf.d/supervisord.conf
ENTRYPOINT ["/var/www/html/docker-entrypoint.sh"]
